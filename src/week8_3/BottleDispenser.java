package week8_3;


import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

public class BottleDispenser {
    private float money;
    private List<Bottle> bottleArray;

    private static BottleDispenser instance = null;

    public static BottleDispenser getInstance() {
        if (instance == null) {
            instance = new BottleDispenser();
            System.out.println("Made new BottleDispenser instance");
        }
        return instance;
    }

    private BottleDispenser() {

        money = 0;

        bottleArray = new ArrayList<Bottle>();
        bottleArray.add(new Bottle(null, 0, 0));
        bottleArray.add(new Bottle("Pepsi Max", 1.5f, 2.2f));
        bottleArray.add(new Bottle("Coca-Cola Zero", 0.5f, 2.0f));
        bottleArray.add(new Bottle("Coca-Cola Zero", 1.5f, 2.5f));
        bottleArray.add(new Bottle("Fanta Zero", 0.5f, 1.95f));

    }

    public void addMoney( double m) {
        money += m;
        System.out.println("Klink! Lisää rahaa laitteeseen!");
    }
    public String buyBottle(int num) {
        Bottle bottle = bottleArray.get(num);

        if (money < bottle.getPrice()) {
            System.out.println("Syötä rahaa ensin!");
            return "nope";
        }
        else if (bottleArray.size() < 1) {
            System.out.println("out");
        }
        else {

            System.out.println("KACHUNK! " + bottle.getName() + " tipahti masiinasta!");

            bottleArray.remove(bottle);
            money -= bottle.getPrice();
        }
        return bottle.getName();
    }
    public float returnMoney() {
        money = Math.round(money * 100f)/100f;
        float retmoney = money;

        DecimalFormat df = new DecimalFormat("#####0.00");
        DecimalFormatSymbols dfs = df.getDecimalFormatSymbols();

        dfs.setDecimalSeparator(',');
        df.setDecimalFormatSymbols(dfs);

        System.out.println("Klink klink. Sinne menivät rahat! Rahaa tuli ulos " + df.format(money) + "€");

        money = 0;

        return retmoney;
    }

    public void list() {
        int cnt = 0;
        for (Bottle bottle: bottleArray) {
            cnt++;
            System.out.println(cnt + ". Nimi: " + bottle.getName());
            System.out.print("\tKoko: " + bottle.getSize());
            System.out.println("\tHinta: " + bottle.getPrice());
        }
    }

    public List<Bottle> getBottleArray() {
        return bottleArray;
    }
}