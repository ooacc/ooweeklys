package week8_3;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.awt.event.MouseEvent;


public class Controller {

    @FXML
    private TextArea consoleArea;

    @FXML
    private ComboBox<Bottle> bottleCombo;

    @FXML
    private Slider slider;

    @FXML
    private Button returnMoney;

    @FXML
    private Button buyButton;

    @FXML
    private Label label1;

    @FXML
    private Button insertMoney;

    @FXML
    void buyButtonAction(ActionEvent event) {
        String name = BottleDispenser.getInstance().buyBottle(bottleCombo.getSelectionModel().getSelectedIndex());
        if (name.contains("nope")) {
            consoleArea.setText(consoleArea.getText()+ "\nSyötä rahaa ensin!");
            return;
        }
        consoleArea.setText(consoleArea.getText()+ "\nKACHUNK! " + name + " tipahti masiinasta!");

        bottleCombo.getItems().clear();
        bottleCombo.getItems().addAll(BottleDispenser.getInstance().getBottleArray());
    }

    @FXML
    void insertMoneyAction(ActionEvent event) {
        double num = slider.getValue();
        insertMoney.setText(String.valueOf(Math.round(num*100.0)/100.0));
        BottleDispenser.getInstance().addMoney(num);
        consoleArea.setText(consoleArea.getText() + "\nKlink! Lisää rahaa laitteeseen!");
        slider.setValue(0);
        insertMoney.setText("0");
    }

    @FXML
    void returnMoneyAction(ActionEvent event) {
        float money = BottleDispenser.getInstance().returnMoney();
        consoleArea.setText(consoleArea.getText() + "\nKlink klink. Sinne menivät rahat! Rahaa tuli ulos " + money + "€");
    }

    @FXML
    void sliderAction(javafx.scene.input.MouseEvent event) {
        double num = slider.getValue();
        insertMoney.setText(String.valueOf(Math.round(num*100.0)/100.0));
    }

    @FXML
    private void initialize()
    {

        // Populate bottle combobox
        bottleCombo.getItems().addAll(BottleDispenser.getInstance().getBottleArray());

    }
}
