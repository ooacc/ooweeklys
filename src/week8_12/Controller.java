package week8_12;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;

public class Controller {

    @FXML
    private TextArea consoleArea;

    @FXML
    private ComboBox<Bottle> bottleCombo;

    @FXML
    private Button returnMoney;

    @FXML
    private Button buyButton;

    @FXML
    private Label label1;

    @FXML
    private Button insertMoney;

    @FXML
    void buyButtonAction(ActionEvent event) {
        String name = BottleDispenser.getInstance().buyBottle(bottleCombo.getSelectionModel().getSelectedIndex());
        if (name.contains("nope")) {
            consoleArea.setText(consoleArea.getText()+ "\nSyötä rahaa ensin!");
            return;
        }
        consoleArea.setText(consoleArea.getText()+ "\nKACHUNK! " + name + " tipahti masiinasta!");

        bottleCombo.getItems().clear();
        bottleCombo.getItems().addAll(BottleDispenser.getInstance().getBottleArray());
    }

    @FXML
    void insertMoneyAction(ActionEvent event) {
        BottleDispenser.getInstance().addMoney(1);
        consoleArea.setText(consoleArea.getText() + "\nKlink! Lisää rahaa laitteeseen!");
    }

    @FXML
    void returnMoneyAction(ActionEvent event) {
        float money = BottleDispenser.getInstance().returnMoney();
        consoleArea.setText(consoleArea.getText() + "\nKlink klink. Sinne menivät rahat! Rahaa tuli ulos " + money + "€");
    }

    @FXML
    private void initialize()
    {

        // Populate bottle combobox
        bottleCombo.getItems().addAll(BottleDispenser.getInstance().getBottleArray());

    }
}
