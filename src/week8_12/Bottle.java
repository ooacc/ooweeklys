package week8_12;

public class Bottle {
    private String name;
    private float size;
    private String maker;
    private float price;

    public Bottle(String name, float size, float price) {
        if (name != null) {
            this.name = name;
            this.size = size;
            this.price = price;
        }
        else {
            this.name = "Pepsi Max";
            this.size = 0.5f;
            this.maker = "Pepsi";
            this.price = 1.8f;
        }
    }

    public String getName() {
        return name;
    }

    public float getPrice() {
        return price;
    }

    public float getSize() {
        return size;
    }

    @Override
    public String toString() {
        return name;
    }
}
