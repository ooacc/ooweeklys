package week7_14;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.awt.event.KeyEvent;

public class Controller {

    @FXML
    private Label label;

    @FXML
    private Button button1;

    @FXML
    private TextField textField;

    @FXML
    void button1Action(ActionEvent event) {
        System.out.println("Hello world!");
        label.setText(textField.getText());
    }

    @FXML
    public void textFieldAction(ActionEvent event) {
        label.setText(textField.getText());
    }
}

