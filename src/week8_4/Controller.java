package week8_4;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class Controller {

    private List<Float> sizes = new ArrayList<>();
    private List<String> names = new ArrayList<>();

    @FXML
    private TextArea consoleArea;

    @FXML
    private ComboBox<String> bottleSizeCombo;

    @FXML
    private ComboBox<String> bottleCombo;

    @FXML
    private Slider slider;

    @FXML
    private Button returnMoney;

    @FXML
    private Button buyButton;

    @FXML
    private Label label1;

    @FXML
    private Button insertMoney;

    @FXML
    void buyButtonAction(ActionEvent event) {
        String name = BottleDispenser.getInstance().buyBottle(bottleCombo.getSelectionModel().getSelectedItem(), bottleSizeCombo.getSelectionModel().getSelectedItem());
        if (name.contains("money")) {
            consoleArea.setText(consoleArea.getText()+ "\nSyötä rahaa ensin!");
            return;
        }
        if (name.contains("Ei saatavilla")) {
            consoleArea.setText(consoleArea.getText()+ "\nEi saatavilla!");
            return;
        }
        if (name.contains("Kone tyhja")) {
            consoleArea.setText(consoleArea.getText()+ "\nKone tyhja!");
            return;
        }
        consoleArea.setText(consoleArea.getText()+ "\nKACHUNK! " + name + " tipahti masiinasta!");

    }

    @FXML
    void insertMoneyAction(ActionEvent event) {
        double num = slider.getValue();
        insertMoney.setText(String.valueOf(Math.round(num*100.0)/100.0));
        BottleDispenser.getInstance().addMoney(num);
        consoleArea.setText(consoleArea.getText() + "\nKlink! Lisää rahaa laitteeseen!");
        slider.setValue(0);
        insertMoney.setText("0");
    }

    @FXML
    void returnMoneyAction(ActionEvent event) {
        float money = BottleDispenser.getInstance().returnMoney();
        consoleArea.setText(consoleArea.getText() + "\nKlink klink. Sinne menivät rahat! Rahaa tuli ulos " + money + "€");
    }

    @FXML
    void sliderAction(javafx.scene.input.MouseEvent event) {
        double num = slider.getValue();
        insertMoney.setText(String.valueOf(Math.round(num*100.0)/100.0));
    }

    @FXML
    private void initialize()
    {

        // Populate bottle combobox
        for (Bottle bottle : BottleDispenser.getInstance().getBottleArray()) {
            if (!names.contains(bottle.getName())) {
                names.add(bottle.getName());
                bottleCombo.getItems().add(bottle.getName());
            }
        }

        // Populate bottleSize combobox
        for (Bottle bottle : BottleDispenser.getInstance().getBottleArray()) {
            if (!sizes.contains(bottle.getSize())) {
                sizes.add(bottle.getSize());
                bottleSizeCombo.getItems().add(String.valueOf(bottle.getSize()));
            }
        }
    }
}
