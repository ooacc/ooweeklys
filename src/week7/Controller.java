package week7;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;


public class Controller {


    @FXML
    private Button button2;

    @FXML
    private Label label;

    @FXML
    private Button button1;

    @FXML
    private TextField textField;

    @FXML
    private TextArea textArea;

    @FXML
    void button1Action(ActionEvent event) {
        textArea.setText(IO.read(textField.getText()));
    }

    @FXML
    void textFieldAction(ActionEvent event) {

    }

    @FXML
    void button2Action(ActionEvent event) {
        IO.write(textArea.getText(), textField.getText());
    }
}