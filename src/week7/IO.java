package week7;

import java.io.*;


public class IO {
    public static String read( String name) {
        try {
            if (name.isEmpty()) {
                System.out.println("Anna tiedostonnimi");
                return null;
            }
            BufferedReader br = new BufferedReader(new FileReader(name));
            String line = br.readLine();
            String all = "";


            while (line != null) {
                //System.out.println(line);
                all = all + line + "\n";
                line = br.readLine();
            }
            br.close();

            return all;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void write(String write, String name) {

        try {
            if (name.isEmpty()) {
                System.out.println("Anna tiedostonnimi");
                return;
            }

            BufferedWriter bw = new BufferedWriter( new FileWriter(name));

            String[] writeLines = write.split("\n");
            for (String s : writeLines) {
                bw.write(s);
                bw.newLine();
            }


            bw.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
